package demo;

import entity.Fraction;
import service.IFractionService;

public class DemoService implements IDemoService {
    private IFractionService fractionService;

    public DemoService(IFractionService fractionService) {
        this.fractionService = fractionService;
    }

    public void execute() {
        Fraction fraction0 = new Fraction(15, 20);
        checkSimplification(fraction0);

        Fraction fraction1 = new Fraction(5, 7);
        Fraction fraction2 = new Fraction(3, 4);
        checkAddFractions(fraction1, fraction2);
        checkSubtractFractions(fraction1, fraction2);
        checkMultiplyFractions(fraction1, fraction2);
        checkDivideFractions(fraction1, fraction2);
    }

    @Override
    public void checkSimplification(Fraction fraction) {
        System.out.println("------------------------------------------------------------");
        System.out.println(fraction);
        System.out.println("After simplification: " + fractionService.simplification(fraction));
    }

    @Override
    public void checkAddFractions(Fraction fraction1, Fraction fraction2) {
        System.out.println("------------------------------------------------------------");
        System.out.println(fraction1 + " + " + fraction2);
        System.out.println(fractionService.addFractions(fraction1, fraction2));
    }

    @Override
    public void checkSubtractFractions(Fraction fraction1, Fraction fraction2) {
        System.out.println("------------------------------------------------------------");
        System.out.println(fraction1 + " - " + fraction2);
        System.out.println(fractionService.subtractFractions(fraction1, fraction2));
    }

    @Override
    public void checkMultiplyFractions(Fraction fraction1, Fraction fraction2) {
        System.out.println("------------------------------------------------------------");
        System.out.println(fraction1 + " * " + fraction2);
        System.out.println(fractionService.multiplyFractions(fraction1, fraction2));
    }

    @Override
    public void checkDivideFractions(Fraction fraction1, Fraction fraction2) {
        System.out.println("------------------------------------------------------------");
        System.out.println(fraction1 + " / " + fraction2);
        System.out.println(fractionService.divideFractions(fraction1, fraction2));
    }
}
