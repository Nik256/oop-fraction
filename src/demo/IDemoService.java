package demo;

import entity.Fraction;

public interface IDemoService {
    void checkSimplification(Fraction fraction);
    void checkAddFractions(Fraction fraction1, Fraction fraction2);
    void checkSubtractFractions(Fraction fraction1, Fraction fraction2);
    void checkMultiplyFractions(Fraction fraction1, Fraction fraction2);
    void checkDivideFractions(Fraction fraction1, Fraction fraction2);
}
