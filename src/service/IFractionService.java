package service;

import entity.Fraction;

public interface IFractionService {
    Fraction simplification(Fraction fraction);
    Fraction addFractions(Fraction fraction1, Fraction fraction2);
    Fraction subtractFractions(Fraction fraction1, Fraction fraction2);
    Fraction multiplyFractions(Fraction fraction1, Fraction fraction2);
    Fraction divideFractions(Fraction fraction1, Fraction fraction2);
}
