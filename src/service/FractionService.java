package service;

import entity.Fraction;

public class FractionService implements IFractionService {
    private int getGreatestCommonDivisor(int a, int b) {
        if ((a % b) == 0) {
            return b;
        } else {
            return getGreatestCommonDivisor(b, a % b);
        }
    }

    @Override
    public Fraction simplification(Fraction fraction) {
        int numerator = fraction.getNumerator();
        int denominator = fraction.getDenominator();
        int gcd = this.getGreatestCommonDivisor(numerator, denominator);
        if (gcd < 0)
            gcd = -gcd;
        numerator = numerator / gcd;
        denominator = denominator / gcd;
        return new Fraction(numerator, denominator);
    }

    @Override
    public Fraction addFractions(Fraction fraction1, Fraction fraction2) {
        int numerator3 = fraction1.getNumerator() * fraction2.getDenominator() + fraction2.getNumerator() * fraction1.getDenominator();
        int denominator3 = fraction1.getDenominator() * fraction2.getDenominator();
        return simplification(new Fraction(numerator3, denominator3));
    }

    @Override
    public Fraction subtractFractions(Fraction fraction1, Fraction fraction2) {
        int numerator3 = fraction1.getNumerator() * fraction2.getDenominator() - fraction2.getNumerator() * fraction1.getDenominator();
        int denominator3 = fraction1.getDenominator() * fraction2.getDenominator();
        return simplification(new Fraction(numerator3, denominator3));
    }

    @Override
    public Fraction multiplyFractions(Fraction fraction1, Fraction fraction2) {
        int numerator3 = fraction1.getNumerator() * fraction2.getNumerator();
        int denominator3 = fraction1.getDenominator() * fraction2.getDenominator();
        return simplification(new Fraction(numerator3, denominator3));
    }

    @Override
    public Fraction divideFractions(Fraction fraction1, Fraction fraction2) {
        int numerator3 = fraction1.getNumerator() * fraction2.getDenominator();
        int denominator3 = fraction1.getDenominator() * fraction2.getNumerator();
        return simplification(new Fraction(numerator3, denominator3));
    }
}
