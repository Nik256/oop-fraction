import demo.DemoService;
import service.FractionService;

public class Main {

    public static void main(String[] args) {
        new DemoService(new FractionService()).execute();
    }
}
